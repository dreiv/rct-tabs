import React, { useState } from 'react'

import { Tab } from './tab'

import './Tabs.scss'

const Tabs = ({ activeTabIdx: activeIdx = 0, children = [] }) => {
	const [activeTabIdx, setActiveTabIdx] = useState(activeIdx)

	return (
		<>
			<ul className="tab-list">
				{children.map(({ props: { label } }, idx) => (
					<Tab
						key={idx}
						label={label}
						onClick={() => setActiveTabIdx(idx)}
						isActive={activeTabIdx === idx}
					/>
				))}
			</ul>

			{children.length > 0 && children[activeTabIdx].props.children}
		</>
	)
}

export default Tabs
