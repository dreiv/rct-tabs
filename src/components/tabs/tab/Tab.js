import React, { memo } from 'react'

import './Tab.scss'

const Tab = memo(({ label, onClick, isActive }) => {
	let className = 'tab-list-item'

	if (isActive) {
		className += ' active'
	}

	return (
		<li className={className} onClick={onClick}>
			{label}
		</li>
	)
})

export default Tab
