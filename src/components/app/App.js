import React from 'react'

import { Tabs } from 'components/tabs'
import { Tab } from 'components/tabs/tab'

import './App.scss'

const App = () => (
	<>
		<h1>Tabs demo:</h1>

		<Tabs>
			<Tab label="Gator">
				See ya later, <em>Alligator</em>!
			</Tab>

			<Tab
				label={
					<>
						Croc <button>:D</button>
					</>
				}
			>
				After 'while, <em>Crocodile</em>!
			</Tab>

			<Tab label="Sarcosuchus">
				Nothing to see here, this tab is <em>extinct</em>!
			</Tab>
		</Tabs>
	</>
)

export default App
